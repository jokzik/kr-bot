import {Telegraf} from "telegraf";
require('dotenv').config();
console.log(process.env.TELEGRAM_API_TOKEN);
if(typeof process.env.TELEGRAM_API_TOKEN == "string" ) {
    const bot = new Telegraf(process.env.TELEGRAM_API_TOKEN);
    bot.start((ctx) => ctx.reply('Welcome')) //ответ бота на команду /start
    bot.help((ctx) => ctx.reply('Send me a sticker')) //ответ бота на команду /help
    bot.on('sticker', (ctx) => ctx.reply('1')) //bot.on это обработчик введенного юзером сообщения, в данном случае он отслеживает стикер, можно использовать обработчик текста или голосового сообщения
    bot.hears('hi', (ctx) => ctx.reply('Hey there')) // bot.hears это обработчик конкретного текста, данном случае это - "hi"
    bot.launch()
}
